#ifndef NETWORKINFO_H
#define NETWORKINFO_H

#include <cstdint>
#include <string_view>

/**
 * @brief Represents pair of source and destination IP.
 *
 * Both IPs are stored as 32-bit uint.
 *
 * @warning IPs are stored in network order.
 *
 * To obtain IP in host architecture endianness:
 * @code
 * uint16_t = ntohs(ipAddr.source_ip);
 * @endcode
 *
 * To convert to c-string:
 * @code
 * char * ip_as_string = inet_ntoa(ipAddr.source_ip);
 * @endcode
 *
 * @see https://linux.die.net/man/3/ntohs
 * @see https://linux.die.net/man/3/inet_ntoa
 */
struct IpAddr {
  uint32_t source_ip; /**< Source IP stored in network order. */
  uint32_t dest_ip;   /**< Destination IP stored in network order. */
  /**
   * @brief Construct a new IpAddr object from character sequence.
   *
   * std::string_view is capable of implicite and cheap conversion from
   * std::string and c-string.
   *
   * @param[in] src_ip Source IP represented as char sequence.
   * @param[in] dst_ip Destination IP represented as char sequence.
   *
   * @warning Passing IP address as std::string or char[] rely on std::string's
   * internal buffer being null-terminated aswell as char[], if null-terminator
   * is not appended then it's undefined behaviour.
   *
   * @see https://en.cppreference.com/w/cpp/string/basic_string_view
   */
  IpAddr(std::string_view src_ip, std::string_view dst_ip);
};

/**
 * @brief Represents pair of source and destination ports.
 *
 * Both ports are represented as 16-bit uint.
 *
 * @warning Ports are stored in network order.
 *
 * To convert ports to host architecture endianness:
 * @code
 * uint16_t port = ntohs(portInfo.source_dest);
 * @endcode
 *
 * @see https://linux.die.net/man/3/ntohs
 */
struct PortInfo {
  uint16_t source_port; /**< Source port stored in network order. */
  uint16_t dest_port;   /**< Destination port stored in network order. */
  /**
   * @brief Construct a new PortInfo object
   *
   * @param[in] src_port Source port in host order.
   * @param[in] dst_port Destination port in host order.
   */
  PortInfo(uint16_t src_port, uint16_t dst_port);
};

/**
 * @brief Collective information about IP addresses, ports and MTU.
 * @see https://en.wikipedia.org/wiki/Maximum_transmission_unit
 */
struct NetworkInfo : public IpAddr, public PortInfo {
  int MTU;  //< Maximum Transmission Unit used to calculate packet size.
  /**
   * @brief Construct a new NetworkInfo and aggregated structures IpAddr and
   * PortInfo.
   *
   * @param[in] src_ip Source IP as char sequence.
   * @param[in] dst_ip Destination IP as char sequence.
   * @param[in] src_port Source port.
   * @param[in] dst_port Destination port.
   * @param[in] mtu Maximum Transmission Unit.
   */
  NetworkInfo(std::string_view src_ip, std::string_view dst_ip,
              uint16_t src_port, uint16_t dst_port, int mtu);
};
#endif
