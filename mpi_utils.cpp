#include "mpi_utils.h"

#include "fragments.h"

auto UtilsMPI::broadcast_destination_address(sockaddr_in& address)
    -> MPI_Request {
  constexpr int ROOT_ID = 0;
  MPI_Request request{};
  MPI_Ibcast(&address, sizeof(address), MPI_BYTE, ROOT_ID, MPI_COMM_WORLD,
             &request);
  return request;
}
