#include "relay.h"
#include <sys/socket.h>
#include "mpi_utils.h"
#ifdef DEBUG
#include <iostream>
#endif

auto Relay::recv_destination_addr(sockaddr_in& socket_addr) const
    -> MPI_Request {
  return UtilsMPI::broadcast_destination_address(socket_addr);
}

auto Relay::set_socket_descriptor(int socket_descriptor) -> void {
  if (socket_descriptor < 0) {
#ifdef DEBUG
    std::cout << "\033[1;34m[RELAY #" << relay_id
              << "]\033[0m Socket set to invalid value\n";
#endif
    socket_fd = {};
  }

  else {
#ifdef DEBUG
    std::cout << "\033[1;34m[RELAY #" << relay_id
              << "]\033[0m Socket descriptor set\n";
#endif
    socket_fd = socket_descriptor;
  }
}

auto Relay::recv_fragments() -> void {
  constexpr int ROOT_INDEX = 0;

  int fragments_number = 0;
  MPI_Recv(&fragments_number, 1, MPI_INT, ROOT_INDEX, 0, MPI_COMM_WORLD,
           nullptr);
  fragments_buffer.resize(fragments_number);
#ifdef DEBUG
  std::cout << "\033[1;34m[RELAY #" << relay_id << "]\033[0m Receiving "
            << fragments_number << " fragments\n";
#endif
  for (int i = 0; i < fragments_number; ++i)
    Fragment::recv_fragment(fragments_buffer[i]);
}

auto Relay::forward_fragments(sockaddr_in& socket_addr) const -> void {
#ifdef DEBUG
  std::cout << "\033[1;34m[RELAY #" << relay_id
            << "]\033[0m Frowarding framents to destination\n";
#endif
  Expects(socket_fd.has_value());

  for (const auto& fragment : fragments_buffer) {
    sendto(socket_fd.value(), fragment.data().data(), fragment.data().size(), 0,
           reinterpret_cast<const sockaddr*>(&socket_addr),
           sizeof(socket_addr));
  }
#ifdef DEBUG
  std::cout << "\033[1;34m[RELAY #" << relay_id << "]\033[0m Done forwarding\n";
#endif
}

auto Relay::recv_and_forward_fragments() -> void {
  sockaddr_in socket_addr{};
  MPI_Request request = recv_destination_addr(socket_addr);

#ifdef DEBUG
  std::cout << "\033[1;34m[RELAY #" << relay_id
            << "]\033[0m Receiving fragments from host\n";
#endif
  recv_fragments();

  MPI_Wait(&request, MPI_STATUS_IGNORE);
#ifdef DEBUG
  std::cout << "\033[1;34m[RELAY #" << relay_id
            << "]\033[0m Received socket info. Frowarding fragments..\n";
#endif
  forward_fragments(socket_addr);
}

Relay::Relay(int id) : relay_id(id), socket_fd({}) {}
