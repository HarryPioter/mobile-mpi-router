#include "fragments.h"
#include <algorithm>
#include <iterator>
#include <numeric>
#include "checksum.h"

namespace {

auto reserve_memory_for_fragments(std::vector<Fragment>& fragments,
                                  std::ptrdiff_t payload_size, int mtu)
    -> void {
  auto packet_payload_size = mtu - sizeof(iphdr);
  auto packet_count = payload_size / packet_payload_size;
  auto last_fragment_payload = payload_size % packet_payload_size;
  auto elements_to_reserve =
      last_fragment_payload > 0 ? packet_count + 1 : packet_count;

  fragments.resize(elements_to_reserve);
}

}  // namespace

auto Fragment::ip_header() -> iphdr* {
  return reinterpret_cast<iphdr*>(datagram.data());
}

auto Fragment::data() const -> gsl::span<const gsl::byte> { return datagram; }

auto Fragment::resize(int payload_size) -> void {
  datagram.resize(payload_size + sizeof(iphdr));
}

auto Fragment::calculate_checksum() -> void {
  auto iph = ip_header();
  iph->check = 0;
  auto chsum = checksum(datagram);
  iph->check = htons(chsum);
}

auto Fragment::assign_data(const iphdr& header,
                           gsl::span<const gsl::byte> payload) -> void {
  resize(payload.size());
  auto iph = ip_header();
  *iph = header;
  iph->tot_len = htons(static_cast<uint16_t>(datagram.size()));
  std::copy(std::cbegin(payload), std::cend(payload),
            std::begin(datagram) + sizeof(iphdr));
  calculate_checksum();
}

auto generate_fragments(Fragment::PacketInfo info, int mtu,
                        std::vector<Fragment>& fragments) -> void {
  Expects(mtu > 0);
  const int payload_per_packet = mtu - sizeof(iphdr);
  Expects(payload_per_packet % 8 == 0);

  const auto& payload = info.data_payload;
  const auto bytes_count = payload.size_bytes();
  reserve_memory_for_fragments(fragments, bytes_count, mtu);

  auto& header = info.header;
  std::accumulate(
      std::begin(fragments), std::end(fragments), uint16_t{0},
      [&](uint16_t offset, Fragment& frag) {
        if (auto tail_bytes = bytes_count - offset;
            tail_bytes <= payload_per_packet) {
          header.frag_off = htons(offset / 8);
          frag.assign_data(header, payload.subspan(offset, tail_bytes));
        } else {
          header.frag_off = htons(IP_MF | (offset / 8));
          frag.assign_data(header, payload.subspan(offset, payload_per_packet));
        }

        return offset + payload_per_packet;
      });
}

auto Fragment::recv_fragment(Fragment& fragment) -> void {
  constexpr int32_t ROOT_ID = 0;

  int fragment_size = 0;
  MPI_Recv(&fragment_size, 1, MPI_INT, ROOT_ID, 0, MPI_COMM_WORLD, nullptr);
  fragment.datagram.resize(fragment_size);

  MPI_Recv(fragment.datagram.data(), fragment_size, MPI_BYTE, ROOT_ID, 0,
           MPI_COMM_WORLD, nullptr);
}
