#ifndef RELAY_H
#define RELAY_H

#include <optional>
#include "fragments.h"

/**
 * @brief Receives IPv4 packet fragments and forwards them to destination.
 *
 */
class Relay {
 private:
  int relay_id;
  std::optional<int> socket_fd;
  std::vector<Fragment> fragments_buffer;

  auto recv_destination_addr(sockaddr_in& socket_addr) const -> MPI_Request;
  auto recv_fragments() -> void;
  auto forward_fragments(sockaddr_in& socket_addr) const -> void;

 public:
  /**
   * @brief Construct a new Relay object
   *
   * @param id ID of process withing MPI_COMM_WORLD communicator.
   */
  Relay(int id);

  /**
   * @brief Receives and forwards fragments received from Host to destination
   * provided in Host controll messages.
   *
   */
  auto recv_and_forward_fragments() -> void;

  /**
   * @brief Set the socket descriptor
   *
   * @param socket_descriptor to created socket, if -1 socket is set to {}
   */
  auto set_socket_descriptor(int socket_descriptor) -> void;
};

#endif
