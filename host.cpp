#include "host.h"
#include <mpi.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <cmath>
#include <limits>
#include <numeric>
#include "checksum.h"
#include "mpi_utils.h"
#include "network_info.h"
#ifdef DEBUG
#include <arpa/inet.h>
#include <iostream>
#endif

namespace {

struct pseudo_header {
  u_int32_t source_address;
  u_int32_t dest_address;
  u_int8_t placeholder;
  u_int8_t protocol;
  u_int16_t tcp_length;
};

using DataPart = Host::DataPart;
auto partition_fragments_between_relays(gsl::span<const Fragment> fragments,
                                        gsl::span<const double> relay_weights,
                                        std::vector<DataPart>& relay_tasks)
    -> void {
  const double weights_sum = std::accumulate(std::cbegin(relay_weights),
                                             std::cend(relay_weights), 0.0);

  relay_tasks.clear();

  int offset = 0;
  for (int i = 0; i < relay_tasks.size() - 1; ++i) {
    const double relays_workload_fraction = relay_weights[i] / weights_sum;
    int chunk_size = static_cast<int>(
        std::ceil(fragments.size() * relays_workload_fraction));
    relay_tasks.push_back(DataPart{offset, fragments.size(), chunk_size});
    offset += chunk_size;
  }
  relay_tasks.push_back(
      DataPart{offset, fragments.size(), fragments.size() - offset});

  Ensures(relay_tasks.size() == relay_weights.size());
}

auto send_fragment(int relay_id, const Fragment& fragment) -> void {
  const int fragment_size = fragment.data().size();
  MPI_Send(&fragment_size, 1, MPI_INT, relay_id, 0, MPI_COMM_WORLD);

  MPI_Send(fragment.data().data(), fragment_size, MPI_BYTE, relay_id, 0,
           MPI_COMM_WORLD);
}

auto fill_pseudoheader(pseudo_header* psh, const IpAddr& info,
                       uint16_t tcp_segment_size) -> void {
  psh->source_address = info.source_ip;
  psh->dest_address = info.dest_ip;
  psh->placeholder = 0;
  psh->protocol = IPPROTO_TCP;
  psh->tcp_length = htons(tcp_segment_size);
}

constexpr auto generate_tcp_header_invariant() -> tcphdr {
  tcphdr tcph{};

  tcph.seq = 0;
  tcph.ack_seq = 0;
  tcph.doff = 5;  // tcp header size
  tcph.fin = 0;
  tcph.syn = 0;
  tcph.rst = 0;
  tcph.psh = 0;
  tcph.ack = 0;
  tcph.urg = 0;
  tcph.check = 0;
  tcph.urg_ptr = 0;
  tcph.check = 0;

  return tcph;
}

auto fill_tcp_header(tcphdr* tcph, const PortInfo& info) -> void {
  *tcph = generate_tcp_header_invariant();
  tcph->source = info.source_port;
  tcph->dest = info.dest_port;
  tcph->window = htons(5840); /* maximum allowed window size */
}

auto prepend_tcp_header(gsl::span<const std::byte> data,
                        std::vector<std::byte>& internal_data_buffer,
                        const NetworkInfo& info) -> gsl::span<const std::byte> {
  const int bytes_count = sizeof(pseudo_header) + sizeof(tcphdr) + data.size();
  internal_data_buffer.resize(bytes_count);

  const auto tcp_segment_size =
      static_cast<uint16_t>(bytes_count - sizeof(pseudo_header));
  auto psh = reinterpret_cast<pseudo_header*>(internal_data_buffer.data());
  fill_pseudoheader(psh, info, tcp_segment_size);

  auto tcph = reinterpret_cast<tcphdr*>(internal_data_buffer.data() +
                                        sizeof(pseudo_header));
  fill_tcp_header(tcph, info);

  std::copy(std::cbegin(data), std::cend(data),
            std::begin(internal_data_buffer) + sizeof(pseudo_header) +
                sizeof(tcphdr));
  tcph->check = checksum(internal_data_buffer);

  return gsl::make_span(internal_data_buffer.data() + sizeof(pseudo_header),
                        tcp_segment_size);
}

constexpr auto generate_ip_header_invariant() -> iphdr {
  iphdr iph{};

  iph.ihl = 5;
  iph.version = 4;
  iph.tos = 0;
  iph.tot_len = 0;
  iph.frag_off = 0;
  iph.ttl = 255;
  iph.protocol = IPPROTO_TCP;
  iph.check = 0;

  return iph;
}

auto create_blank_ip_header(const IpAddr& info, uint16_t packet_id) -> iphdr {
  iphdr iph = generate_ip_header_invariant();

  iph.id = htons(packet_id);
  iph.saddr = info.source_ip;
  iph.daddr = info.dest_ip;

  return iph;
}

auto create_socket_info(const NetworkInfo& info) -> sockaddr_in {
  sockaddr_in sock_info{};
  sock_info.sin_family = AF_INET;
  sock_info.sin_port = info.dest_port;
  sock_info.sin_addr.s_addr = info.dest_ip;

  return sock_info;
}

}  // namespace

auto Host::DataPart::work_items_number(int data_size) const -> int {
  int work_items = 0;
  for (int i = start; i < data_size; i += offset) {
    int distance_to_end = data_size - i;
    if (distance_to_end < chunk_size) {
      work_items += distance_to_end;
      break;
    } else
      work_items += chunk_size;
  }

  return work_items;
}

auto Host::send_fragments_to_relays() const -> void {
  //#pragma omp parallel for
  for (gsl::index i = 0; i < tasks.size(); ++i) {
    const auto& task = tasks[i];
    const int relay_id = i + 1;

    const auto fragment_count = fragments_buffer.size();
    int fragments_to_send_count = task.work_items_number(fragment_count);
    MPI_Send(&fragments_to_send_count, 1, MPI_INT, relay_id, 0, MPI_COMM_WORLD);

    for (gsl::index j = task.start; j < fragment_count; j += task.offset) {
      for (gsl::index k = 0; k < task.chunk_size && (j + k) < fragment_count;
           ++k) {
        const auto& fragment = fragments_buffer[j + k];
        send_fragment(relay_id, fragment);
      }
    }
  }
}

auto Host::send(gsl::span<const std::byte> data, const NetworkInfo& info)
    -> void {
  gsl::span<const std::byte> tcp_segment =
      prepend_tcp_header(data, data_buffer, info);
  Fragment::PacketInfo pckt_info{
      tcp_segment, create_blank_ip_header(info, uni_distrib(bits_engine))};

  sockaddr_in sock_info = create_socket_info(info);
#ifdef DEBUG
  std::cout << "\033[1;32m[HOST]\033[0m Broadcasting socket info:\n"
            << "dest IP: " << inet_ntoa(sock_info.sin_addr)
            << "\ndest port: " << ntohs(sock_info.sin_port)
            << "\nMTU: " << info.MTU << '\n';
#endif
  MPI_Request request = UtilsMPI::broadcast_destination_address(sock_info);

  generate_fragments(pckt_info, info.MTU, fragments_buffer);
#ifdef DEBUG
  std::cout << "\033[1;32m[HOST]\033[0m Generated " << fragments_buffer.size()
            << " IP fragments\n";
#endif
  partition_fragments_between_relays(fragments_buffer, weights_buffer, tasks);
#ifdef DEBUG
  for (gsl::index i = 0; i < tasks.size(); ++i) {
    std::cout << "\033[1;32m[HOST]\033[0m Task#" << i + 1
              << " fragments to send: "
              << tasks[i].work_items_number(fragments_buffer.size()) << '\n';
  }
  std::cout << "\033[1;32m[HOST]\033[0m Sending fragments to relays\n";
#endif
  MPI_Wait(&request, MPI_STATUS_IGNORE);
  send_fragments_to_relays();
#ifdef DEBUG
  std::cout << "\033[1;32m[HOST]\033[0m Sending fragments to relays done\n";
#endif
}

Host::Host(int world_size)
    : communicator_size(world_size),
      uni_distrib(1, std::numeric_limits<uint16_t>().max()) {
  const int relays_number = world_size - 1;
  Expects(relays_number > 0);
  weights_buffer.reserve(relays_number);
  tasks.reserve(relays_number);

  // TODO replace with signal strength weights
  for (int i = 0; i < relays_number; ++i) weights_buffer.push_back(1);
}
