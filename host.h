#ifndef HOST_H
#define HOST_H
#include <random>
#include "fragments.h"

class NetworkInfo;

/**
 * @brief Generates IPv4 packet fragments and distributes them between Relays
 * based on their signal strength.
 *
 */
class Host {
 public:
  /**
   * @brief Represents part of continous work-items container.
   * Concept based on loop-scheduling strategies used in OpenMP.
   *
   * @see https://software.intel.com/en-us/articles/openmp-loop-scheduling
   */
  struct DataPart {
    int start;      /**< Index of first data part. */
    int offset;     /**< Number of indices to jump after iteration through
                     * DataPart#chunk_size items. */
    int chunk_size; /**< Number of indices in contignous container block. */

    /**
     * @brief Counts number of indices to process for given container size.
     *
     * @param[in] data_size Size of container holding data.
     * @return int Number of indices to process.
     */
    auto work_items_number(int data_size) const -> int;
  };

 private:
  int communicator_size;
  std::vector<double> weights_buffer;
  std::vector<Fragment> fragments_buffer;
  std::vector<DataPart> tasks;
  std::vector<std::byte> data_buffer;

  // Packet ID generation
  std::minstd_rand0 bits_engine;
  std::uniform_int_distribution<uint16_t> uni_distrib;

  auto send_fragments_to_relays() const -> void;

 public:
  /**
   * @brief Construct a new Host object
   *
   * @param world_size Size of MPI_COMM_WORLD.
   */
  Host(int world_size);

  /**
   * @brief Generates IPv4 packet fragments for given payload and provided
   * network destination and sends them to Relays.
   *
   * @param[in] data Data to be sent through network.
   * @param[in] info Source, destination and IP layer information.
   */
  auto send(gsl::span<const std::byte> data, const NetworkInfo& info) -> void;
};

#endif
