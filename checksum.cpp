#include "checksum.h"

auto checksum(gsl::span<const std::byte> bytes) -> uint16_t {
  const auto* buf = bytes.data();
  uint32_t sum = 0;

  /* Accumulate checksum */
  int32_t i;
  for (i = 0; i < bytes.size() - 1; i += 2) {
    uint16_t word16 = *(uint16_t*)&buf[i];
    sum += word16;
  }

  /* Handle odd-sized case */
  if (bytes.size() & 1) {
    sum += std::to_integer<uint16_t>(buf[i]);
  }

  /* Fold to get the ones-complement result */
  while (sum >> 16) sum = (sum & 0xFFFF) + (sum >> 16);

  /* Invert to get the negative in ones-complement arithmetic */
  return ~sum;
}