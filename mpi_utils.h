/**
 * @file mpi_utils.h
 * @brief Utility functions encapsulating MPI communication.
 *
 */

#ifndef MPI_UTILS_H
#define MPI_UTILS_H

#include <mpi.h>

class sockaddr_in;
class Fragment;

namespace UtilsMPI {

/**
 * @fn auto broadcast_destination_address(sockaddr_in& address) -> MPI_Request
 *
 * @brief Broadcasts socket address within MPI communicator.
 *
 * @param[in, out] address Socket address to be broadcasted, broadcasting
 * process reads socket address object byte-by-byte where receiving processes
 * write socket address into given address.
 *
 * @return MPI_Request MPI request for later synchronization.
 */
auto broadcast_destination_address(sockaddr_in& address) -> MPI_Request;
}  // namespace UtilsMPI

#endif
