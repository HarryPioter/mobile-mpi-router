#ifndef FRAGMENTS_H
#define FRAGMENTS_H

#include <netinet/ip.h>
#include <cstddef>
#include <gsl/span>
#include <vector>
#include "mpi_utils.h"

/**
 * @brief Represents IP packet fragment used inf IP packet fragmetation.
 *
 */
class Fragment {
 public:
  /**
   * @brief Encapsulates reception of IPv4 packet fragment via MPI_COMM_WORLD.
   *
   * @param[out] fragment Fragment to bo written with received data.
   */
  static auto recv_fragment(Fragment& fragment) -> void;

 private:
  std::vector<std::byte> datagram;
  auto resize(int payload_size) -> void;
  auto calculate_checksum() -> void;

 public:
  /**
   * @brief Represents pair of data to be sent possibly in multiple IPv4 packets
   * fragments and IPv4 header with information that will not change between
   * multiple packets.
   */
  struct PacketInfo {
    gsl::span<const std::byte>
        data_payload; /**< Byte view of data to be split into IPv4 packets. */
    iphdr header;     /**< Ipv4 header with invariant data for possible multiple
                         packets. */
  };

  /**
   * @brief Obtains pointer to fragment's IPv4 header.
   *
   * @return iphdr* Pointer to fragment's IPv4 header.
   *
   * @warning Calling Fragment#assign_data may lead to internal buffer
   * reallocation which invalidates returned pointer to IP header.
   */
  auto ip_header() -> iphdr*;

  /**
   * @brief Directly creates IPv4 packet fragment by copying header into
   * internal buffer and appending given payload.
   *
   * @param[in] header IPv4 header that will be copied into internal buffer.
   * @param[in] payload IPv4 packet payload that will be appended after header
   * in internal buffer.
   *
   * @note Method allows user to better reuse internal buffer by resizing and
   * reasigning it instead of heap-allocating new buffer.
   *
   * @warning User must ensure that combination of provided header and payload
   * is valid IPv4 packet.
   */
  auto assign_data(const iphdr& header, gsl::span<const std::byte> payload)
      -> void;

  /**
   * @brief Provides byte view to datagram.
   *
   * @return gsl::span<const std::byte> Immutable view of internal buffer
   * holding IPV4 packet.
   *
   * @warning Calling Fragment#assign_data may lead to internal buffer
   * reallocation which invalidates returned IP packet view.
   */
  auto data() const -> gsl::span<const std::byte>;
};

/**
 * @brief Generates IP packet fragment for specified network info, payload and
 * MTU.
 *
 * @param[in] info Holds data to be sent and IP header info that remain
 * unchanged for all IPv4 packet fragmets.
 * @param[in] mtu Maximum Transsmission Unit describes maximum packet size to be
 * transmitted across network.
 * @param[out] fragments Buffer to which generated fragments are written.
 *
 * @see https://en.wikipedia.org/wiki/IP_fragmentation
 */
auto generate_fragments(Fragment::PacketInfo info, int mtu,
                        std::vector<Fragment>& fragments) -> void;

#endif
