#include "network_info.h"
#include <arpa/inet.h>

IpAddr::IpAddr(std::string_view src_ip, std::string_view dst_ip)
    : source_ip(inet_addr(src_ip.data())), dest_ip(inet_addr(dst_ip.data())) {}

PortInfo::PortInfo(uint16_t src_port, uint16_t dst_port)
    : source_port(htons(src_port)), dest_port(htons(dst_port)) {}

NetworkInfo::NetworkInfo(std::string_view src_ip, std::string_view dst_ip,
                         uint16_t src_port, uint16_t dst_port, int mtu)
    : IpAddr(src_ip, dst_ip), PortInfo(src_port, dst_port), MTU(mtu) {}
