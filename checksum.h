#ifndef CHECKSUM_H
#define CHECKSUM_H

#include <cstddef>
#include <gsl/span>

/**
 * @brief Calculates TCP checksum for given byte sequence.
 *
 * @param[in] bytes Byte sequence
 * @return uint16_t 16-bit TCP checksum
 */
auto checksum(gsl::span<const std::byte> bytes) -> uint16_t;

#endif